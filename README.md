# solver-bot

[![pipeline status](https://gitlab.com/tirtow/solver-bot/badges/master/pipeline.svg)](https://gitlab.com/tirtow/solver-bot/commits/master)
[![coverage report](https://gitlab.com/tirtow/solver-bot/badges/master/coverage.svg)](https://gitlab.com/tirtow/solver-bot/commits/master)

solver-bot is a Discord bot that solves mathematical expressions.

## Supported operations
Basic binary operations
```
= 100 * 3 + 9 // (6 - 2) / 6
= 10 % 3
= 2 ** 6
```

Bitwise operations
```
= 1 << 2
= 4 >> 2
= 1 & 3
= 1 | 3
= 1 ^ 3
```

Math library functions
```
acos
acosh
asin
asinh
atan
atanh
atan2
ceil
copysign
cos
cosh
degrees
erf
erfc
exp
expm1
fabs
factorial
floor
fmod
gamma
gcd
hypot
ldexp
lgamma
log
log1p
log10
log2
pow
radians
sin
sinh
sqrt
tan
tanh
trunc
```

Math library constants
```
pi
e
inf
nan
tau
```


## Setup
1. Install requirements
```
$ pip install -r requirements.txt
```

2. Set the bot token
```
$ export SOLVER_BOT_TOKEN=your_token
```

3. Run the bot
```
$ python3 main.py
```

## Testing
First install coverage
```
$ pip install coverage
```

To run the tests use:
```
$ python3 test_solver.py
```

To generate a coverage report as well use:
```
$ coverage run test_solver.py
$ coverage repot
$ coverage html
```
