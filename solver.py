import importlib
from os import listdir
from os.path import dirname, join, realpath
import re

from errors import MathError, InvalidTokenError, MissingArgumentError


class Solver:
    """
    Solver for equations.

    Takes equations in infix notation and solves them. Loads in operators from
    the operators module to use to solve equations.
    """

    def __init__(self):
        """
        Loads in an instance of each Operator
        """

        self.operators = self._load_operators()

    def solve(self, equation):
        """
        Solves the equation

        equation    -- the equation to solve
        """

        operands = []

        # Solve each operator
        for operator in self._infix_to_postfix(equation):
            try:
                operands.append(operator.solve(operands))
            except IndexError:
                raise MissingArgumentError("Missing an argument for `" +
                                           operator.operation + "`")

        # The only value in operands should be the solution
        if len(operands) > 1:
            raise MathError("Unused operands remaining: '" +
                            str(operands) + "'")

        answer = round(operands.pop(), 10)
        if answer == 0:
            return 0
        return answer

    def _infix_to_postfix(self, equation):
        """
        Converts this equation from infix notation to postfix notation

        equation    -- the equation to convert

        Returns the postfix queue
        """

        queue = []
        stack = []

        # Convert each token into postfix
        for token in self._tokenized(equation):
            token.to_postfix(queue, stack)

        # Move the rest of the stack to the queue
        while len(stack) > 0:
            queue.append(stack.pop())

        return queue

    def _tokenized(self, equation):
        """
        Splits the equation into its tokens

        equation    -- the equation to tokenize

        Returns a generator over the tokens
        """

        # Preprocess the equation
        equation = self._preprocess_equation(equation)

        # Tokenize the equation
        while len(equation) > 0:
            for operator in self.operators:
                # Try to find the operator
                match = re.match(operator.token(), equation)

                if match is not None:
                    # Found the operator that matches the next token
                    yield type(operator)(match.group())
                    equation = equation[match.end():]
                    break
            else:
                # Invalid token given
                raise InvalidTokenError("Invalid token at start of `" +
                                        equation + "`")

    def _preprocess_equation(self, equation):
        """
        Preprocesses the equation.

        Removes whitespace and converts any subtraction operation to adding a
        negative number.

        equation    -- the equation to preprocess

        Returns the processed equation
        """

        # Remove all whitespace
        equation = re.sub("\\s+", "", equation)

        # If the equation starts with a negative convert it to multiplying by -1
        if equation[0] == "-":
            equation = equation.replace("-", "(0-1)*", 1)

        # Convert any negative number to multiplying by -1
        pattern = "([^0-9)]+)(-)"
        match = re.search(pattern, equation)
        while match is not None:
            before = equation[:match.start()]
            after = equation[match.end():]
            replace = match.groups()[0] + "(0-1)*"
            equation = before + replace + after
            match = re.search(pattern, equation)

        return equation

    def _load_operators(self):
        """
        Uses reflection to get an instance of all Operators.

        Returns a list of the Operators
        """

        result = []
        path = join(dirname(realpath(__file__)), "operators")
        for f in listdir(path):
            if f.endswith("_operator.py"):
                name = f[:-3]               # Get rid of the .py
                split = name.split("_")     # Split to before and after _

                # Import the module
                module = importlib.import_module("operators." + "_".join(split))

                # Append an instance of the Operator
                name = "".join(map(lambda s: s.title(), split))
                result.append(getattr(module, name)("0"))

        return result
