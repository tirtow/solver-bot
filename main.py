import discord
from discord.ext.commands import Bot
import os
import platform
import re

from embed import Embed, Color
from solver import Solver
from utils import log


PREFIX = "="

# The Solver for the bot
solver = None

# Create the discord bot client
client = Bot(description="Bot by tirtow#6252", command_prefix=PREFIX)


@client.event
async def on_ready():
    """
    Performs the startup for the bot.
    """

    global solver

    # Set the playing message
    log("Setting playing message")
    game = discord.Game(name=PREFIX + "help")
    await client.change_presence(activity=game)

    # Initialize the Solver
    solver = Solver()

    _print_startup_message()


@client.event
async def on_message(message):
    """
    Handles checking for commands.

    Checks each message to see if it matches one of the commands that the bot
    responds to handling the command if it finds one.

    message -- the discord.Message that was received
    """

    # Convert to lowercase and strip whitespace
    command = message.content.lower()
    command = re.sub("\\s+", "", command)

    if command == PREFIX + "ping":
        # Ping command to check if bot is alive and responding
        log("Pinging bot")
        await message.channel.send("Pong!")
    elif command == PREFIX + "help":
        # Send the help message
        await _send_help_message(message.channel)
    elif command == PREFIX + "about":
        # Send the about message
        await _send_about_message(message.channel)
    elif command.startswith(PREFIX):
        # Solve the message
        await _solve(message)


async def _solve(message):
    """
    Attempts to solve the given problem.

    Outputs the result if successful or the errors if any.

    message     -- the discord.Message received
    """

    # Get the problem
    problem = message.content[1:]
    log("Solving '" + problem + "'")

    try:
        # Attempt to solve
        answer = solver.solve(problem)
        log("Got answer '" + str(answer) + "'")

        embed = Embed(client.user,
                      Color.SUCCESS,
                      description="```" + problem + " = " + str(answer) + "```")
        await message.channel.send(embed=embed)
    except Exception as e:
        # Something went wrong, alert user
        msg = "Error: " + str(e) + "\n\nIn problem: " + problem
        embed = Embed(client.user,
                      Color.ERROR,
                      description=msg)
        await message.channel.send(embed=embed)


async def _send_help_message(channel):
    """
    Sends the help message.

    channel     -- the discord.Channel to send the message to
    """

    msg = \
        "__Available commands:__\n" \
        "- **" + PREFIX + "help:** display this message\n" \
        "- **" + PREFIX + "about:** display information about this bot\n" \
        "- **" + PREFIX + "*expression*:** solve the given expression\n" \
        "\n" \
        "__Examples:__\n" \
        "```\n" \
        "" + PREFIX + " 100 * 3\n" \
        "" + PREFIX + " log2(64) * (9 + 6)\n" \
        "" + PREFIX + " pi * 4 ** 2\n" \
        "```\n" \
        "\n" \
        "For a full list of supported functions see " \
        "https://tirtow.gitlab.io/solver-bot/"
    embed = Embed(client.user,
                  title="About solver-bot",
                  description=msg,
                  color=Color.INFO)
    await channel.send(embed=embed)


async def _send_about_message(channel):
    """
    Sends the about message.

    channel     -- the discord.Channel to send the message to
    """

    msg = \
        "<@" + str(client.user.id) + "> is a Discord bot developed " \
        "by tirtow to solve math equations. Supports basic binary operators " \
        "as well as most of the Python 3.6 math module.\n\n" \
        "More information: https://tirtow.gitlab.io/solver-bot/\n" \
        "Source code: https://gitlab.com/tirtow/solver-bot\n" \
        "More projects by tirtow: " \
        "https://tirtow.gitlab.io/"
    embed = Embed(client.user,
                  title="About solver-bot",
                  description=msg,
                  color=Color.INFO)
    await channel.send(embed=embed)


def _print_startup_message():
    """
    Prints the startup message.
    """

    print("Current Discord.py Version: {} | Current Python Version: {}"
          .format(discord.__version__, platform.python_version()))
    print("--------")
    print("Use this link to invite {}:".format(client.user.name))
    print("https://discordapp.com/oauth2/authorize?client_id={}&scope=bot"
          "&permissions=8".format(client.user.id))
    print("--------")
    print("--------")
    print("Created by tirtow#6252\n")


# Run the bot
if __name__ == "__main__":
    client.run(os.environ["SOLVER_BOT_TOKEN"])
