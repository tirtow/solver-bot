test:
	python3 test_solver.py

coverage:
	coverage run test_solver.py
	coverage report

coverage-html: coverage
	coverage html

bot:
	python3 main.py

setup:
	pip install -r requirements.txt

clean:
	-rm -rf __pycache__/
	-rm -rf htmlcov/
