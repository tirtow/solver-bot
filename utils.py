from datetime import datetime


def log(message, newline=False):
    """
    Logs the message to the console with the current time

    message     -- the message to log
    newline     -- whether or not to print a blank line (default False).
    """

    print("[" + str(datetime.now()) + "]: " + message)
    if newline:
        print("[" + str(datetime.now()) + "]:")
