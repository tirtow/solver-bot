from operators.operator import Operator


class OperandOperator(Operator):
    """
    Class to implement operands
    """

    def __init__(self, value):
        """
        Sets the value for this Operand

        value   -- the value to set
        """

        if "." in value:
            self.value = float(value)
        else:
            self.value = int(value)

    def to_postfix(self, queue, stack):
        """
        Always append operands to the queue
        """

        queue.append(self)

    def precedence(self):
        """
        Operands have a precedence of 0
        """

        return 0

    def token(self):
        """
        Operands match the "[-]?[.0-9]+" pattern
        """

        return "[-]?[.0-9]+"

    def solve(self, operands):
        """
        Returns the value

        operands    -- the operands to use
        """

        return self.value
