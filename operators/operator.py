class Operator:
    """
    Class that the different Operators should implement in order to be reflected
    and used.
    """

    def __init__(self, value):
        """
        Do nothing on initialize by default
        """

    def __le__(self, other):
        """
        Gets if this Operator is less than or equal to another Operator.

        This Operator is less than or equal to another Operator if its
        precedence is less than or equal to the precedence of the other
        Operator.

        other   -- the other Operator

        Returns true if the precedence of this Operator is the same or less than
        the precedence of other, false otherwise.
        """

        return self.precedence() <= other.precedence()

    def to_postfix(self, queue, stack):
        """
        Converts this Operator to postfix format.

        By default, this pops Operators off the stack and appends them to the
        queue until finds an Operator with a lower precedence than this
        Operator. This Operator is appended to the stack.

        queue   -- the postfix queue
        stack   -- the stack of Operators
        """

        while len(stack) > 0 and self <= stack[-1]:
            queue.append(stack.pop())
        stack.append(self)

    def precedence(self):
        """
        Should be implemented by inheriting classes to set the precedence for
        that operator.
        """

        raise NotImplementedError(
                "precedence must be implemented by inheriting classes")

    def token(self):
        """
        Should be implemented by inheriting classes to set the regular
        expression pattern a token must match to be a token for this operator.
        """

        raise NotImplementedError(
                "token must be implemented by inheriting classes")

    def solve(self, operands):
        """
        Should be implemented by inheriting classes to solve for the operator.

        operands    -- the operands to use
        """

        raise NotImplementedError(
                "solve must be implemented by inheriting classes")
