from operators.operator import Operator

import math


class ConstantOperator(Operator):
    """
    Class to implement the various math constants
    """

    # The supported constants
    constants = {
        "pi": {
            "token": "pi",
            "value": math.pi
        },
        "e": {
            "token": "e",
            "value": math.e
        },
        "inf": {
            "token": "inf",
            "value": math.inf
        },
        "nan": {
            "token": "nan",
            "value": math.nan
        },
        "tau": {
            "token": "tau",
            "value": math.tau
        }
    }

    def __init__(self, value):
        """
        Store the passed in value as the constant

        value   -- the constant
        """

        self.constant = value

    def to_postfix(self, queue, stack):
        """
        Always append constants to the queue
        """

        queue.append(self)

    def precedence(self):
        """
        Constants have a precedence of 0
        """

        return 0

    def token(self):
        """
        Builds the pattern to check for any of the constants
        """

        result = "("
        for (i, op) in enumerate(ConstantOperator.constants.values()):
            if i > 0:
                result += "|"
            result += op["token"] + "(?![0-9a-z])"

        return result + ")"

    def solve(self, operands):
        """
        Returns the constant

        operands    -- the operands to use
        """

        return ConstantOperator.constants[self.constant]["value"]
