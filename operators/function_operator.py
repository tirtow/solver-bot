import math

from operators.operator import Operator


class FunctionOperator(Operator):
    """
    Class to implement the math library functions
    """

    # Mapping of the supported functions to their number of arguments
    operations = {
        "acosh": 1,
        "acos": 1,
        "asinh": 1,
        "asin": 1,
        "atanh": 1,
        "atan2": 2,
        "atan": 1,
        "ceil": 1,
        "copysign": 2,
        "cosh": 1,
        "cos": 1,
        "degrees": 1,
        "erfc": 1,
        "erf": 1,
        "expm1": 1,
        "exp": 1,
        "fabs": 1,
        "factorial": 1,
        "floor": 1,
        "fmod": 2,
        "gamma": 1,
        "gcd": 2,
        "hypot": 2,
        "ldexp": 2,
        "lgamma": 1,
        "log10": 1,
        "log1p": 1,
        "log2": 1,
        "log": 2,
        "pow": 2,
        "radians": 1,
        "sinh": 1,
        "sin": 1,
        "sqrt": 1,
        "tanh": 1,
        "tan": 1,
        "trunc": 1
    }

    def __init__(self, value):
        """
        Store the passed in value as the operation to perform

        value   -- the operation to perform
        """

        self.operation = value

    def precedence(self):
        """
        Function operations have a precedence of 4
        """

        return 4

    def token(self):
        """
        Builds the pattern to check for any of the operations
        """

        result = "("
        for (i, op) in enumerate(FunctionOperator.operations):
            if i > 0:
                result += "|"
            result += op

        return result + ")"

    def solve(self, operands):
        """
        Solves the math function for this FunctionOperator

        operands    -- the operands to use
        """

        # Get all the arguments needed
        args = []
        for i in range(FunctionOperator.operations[self.operation]):
            args.insert(0, operands.pop())

        return math.__dict__[self.operation](*args)
