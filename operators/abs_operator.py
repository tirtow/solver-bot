from operators.operator import Operator


class AbsOperator(Operator):
    """
    Class to implement absolute value
    """

    def precedence(self):
        """
        Absolute value has a precedence of 3
        """

        return 3

    def token(self):
        """
        Absolute value uses the "abs" token
        """

        return "abs"

    def solve(self, operands):
        """
        Solves abs(lhs)

        operands    -- the operands to use
        """

        return abs(operands.pop())
