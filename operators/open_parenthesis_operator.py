from errors import MathError
from operators.operator import Operator


class OpenParenthesisOperator(Operator):
    """
    Class to implement open parenthesis
    """

    def to_postfix(self, queue, stack):
        """
        Always append opening parenthesis to the stack

        queue   -- the postfix queue
        stack   -- the stack of Operators
        """

        stack.append(self)

    def precedence(self):
        """
        Opening parenthesis have a precedence of -1
        """

        return -1

    def token(self):
        """
        Operands match the "(" pattern
        """

        return "[(]"

    def solve(self, operands):
        """
        Should never be called.
        """

        raise MathError(
                "solve should not be called on an OpenParenthesisOperator")
