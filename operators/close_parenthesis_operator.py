from errors import MathError
from operators.operator import Operator
from operators.open_parenthesis_operator import OpenParenthesisOperator


class CloseParenthesisOperator(Operator):
    """
    Class to implement closing parenthesis
    """

    def to_postfix(self, queue, stack):
        """
        Pops Operators off the stack into the queue until finds an opening
        parenthesis.

        queue   -- the postfix queue
        stack   -- the stack of Operators
        """

        # Pop off until reach the first opening parenthesis
        while type(stack[-1]) != OpenParenthesisOperator:
            queue.append(stack.pop())

        # Remove the opening parenthesis
        stack.pop()

    def precedence(self):
        """
        Should never be called.
        """

        raise MathError("precedence should not be called on an " +
                        "CloseParenthesisOperator")

    def token(self):
        """
        Operands match the ")" pattern
        """

        return "[)]"

    def solve(self, operands):
        """
        Should never be called.
        """

        raise MathError(
                "solve should not be called on an CloseParenthesisOperator")
