from operators.operator import Operator


class BinaryOperator(Operator):
    """
    Class to implement binary operations
    """

    # The supported operations
    operations = {
        "+": {
            "token": "[+]",
            "precedence": 1,
            "op": lambda lhs, rhs: lhs + rhs
        },
        "-": {
            "token": "[-]",
            "precedence": 1,
            "op": lambda lhs, rhs: lhs - rhs
        },
        "**": {
            "token": "[*]{2}",
            "precedence": 3,
            "op": lambda lhs, rhs: lhs ** rhs
        },
        "*": {
            "token": "[*]{1}",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs * rhs
        },
        "//": {
            "token": "[/]{2}",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs // rhs
        },
        "/": {
            "token": "[/]{1}",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs / rhs
        },
        "%": {
            "token": "[%]",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs % rhs
        },
        "&": {
            "token": "[&]",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs & rhs
        },
        "|": {
            "token": "[|]",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs | rhs
        },
        "^": {
            "token": "[\\^]",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs ^ rhs
        },
        "<<": {
            "token": "[<]{2}",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs << rhs
        },
        ">>": {
            "token": "[>]{2}",
            "precedence": 2,
            "op": lambda lhs, rhs: lhs >> rhs
        }
    }

    def __init__(self, value):
        """
        Store the passed in value as the operation to perform

        value   -- the operation to perform
        """

        self.operation = value

    def precedence(self):
        """
        Gets the precedence for this operation
        """

        return BinaryOperator.operations[self.operation]["precedence"]

    def token(self):
        """
        Builds the pattern to check for any of the operations
        """

        result = "("
        for (i, op) in enumerate(BinaryOperator.operations.values()):
            if i > 0:
                result += "|"
            result += op["token"]

        return result + ")"

    def solve(self, operands):
        """
        Solves the binary operation

        operands    -- the operands to use
        """

        rhs = operands.pop()
        lhs = operands.pop()
        return BinaryOperator.operations[self.operation]["op"](lhs, rhs)
