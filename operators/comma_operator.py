from errors import MathError
from operators.operator import Operator


class CommaOperator(Operator):
    """
    Class to implement commas.

    Commas are used as delimiters between areguments to function operations and
    do not get parsed into the postfix notation.
    """

    def to_postfix(self, queue, stack):
        """
        Commas are just delimiters, do nothing with them
        """

    def precedence(self):
        """
        Opening parenthesis have a precedence of -1
        """

        raise MathError("solve should not be called on a CommaOperator")

    def token(self):
        """
        Operands match the "," pattern
        """

        return "[,]"

    def solve(self, operands):
        """
        Should never be called.
        """

        raise MathError("solve should not be called on a CommaOperator")
