class MathError(Exception):
    """
    Thrown to signal that a math error occurred
    """


class InvalidTokenError(Exception):
    """
    Thrown to signal that an invalid token was given
    """


class MissingArgumentError(Exception):
    """
    Thrown to signal an argument is missing from an operation
    """
