import math
from unittest import TestCase, main

from errors import MathError, InvalidTokenError, MissingArgumentError
from solver import Solver


class TestSolver(TestCase):
    """
    Test cases for the Solver
    """

    def setUp(self):
        """
        Setup a Solver for each test case
        """

        self.solver = Solver()

    def test_simple(self):
        ans = self.solver.solve("10 + 3 * 10 - 8 / 4")
        self.assertEqual(ans, 38)

    def test_parenthesis(self):
        ans = self.solver.solve("10 * (40 - 36)")
        self.assertEqual(ans, 40)

    def test_invalid_equation(self):
        with self.assertRaises(MathError):
            self.solver.solve("100 (100)")

    def test_int_division(self):
        ans = self.solver.solve("5 // 2")
        self.assertEqual(ans, 2)

    def test_division(self):
        ans = self.solver.solve("5 / 2")
        self.assertEqual(ans, 2.5)

    def test_unknown_token(self):
        with self.assertRaises(InvalidTokenError):
            self.solver.solve("100 unknown 100")

    def test_decimal(self):
        ans = self.solver.solve("1.0 - 0.5")
        self.assertEqual(ans, 0.5)

    def test_abs(self):
        ans = self.solver.solve("abs(-100) - 100")
        self.assertEqual(ans, 0)

    def test_acosh(self):
        ans = self.solver.solve("acosh(100)")
        self.assertEqual(ans, round(math.acosh(100), 10))

    def test_acos(self):
        ans = self.solver.solve("acos(0.5)")
        self.assertEqual(ans, round(math.acos(0.5), 10))

    def test_asinh(self):
        ans = self.solver.solve("asinh(100)")
        self.assertEqual(ans, round(math.asinh(100), 10))

    def test_asin(self):
        ans = self.solver.solve("asin(0.5)")
        self.assertEqual(ans, round(math.asin(0.5), 10))

    def test_atanh(self):
        ans = self.solver.solve("atanh(0.5)")
        self.assertEqual(ans, round(math.atanh(0.5), 10))

    def test_atan(self):
        ans = self.solver.solve("atan(100)")
        self.assertEqual(ans, round(math.atan(100), 10))

    def test_ceil(self):
        ans = self.solver.solve("ceil(100.1)")
        self.assertEqual(ans, round(math.ceil(100.1), 10))

    def test_cosh(self):
        ans = self.solver.solve("cosh(100)")
        self.assertEqual(ans, round(math.cosh(100), 10))

    def test_cos(self):
        ans = self.solver.solve("cos(100)")
        self.assertEqual(ans, round(math.cos(100), 10))

    def test_degrees(self):
        ans = self.solver.solve("degrees(100)")
        self.assertEqual(ans, round(math.degrees(100), 10))

    def test_erfc(self):
        ans = self.solver.solve("erfc(100)")
        self.assertEqual(ans, round(math.erfc(100), 10))

    def test_erf(self):
        ans = self.solver.solve("erf(100)")
        self.assertEqual(ans, round(math.erf(100), 10))

    def test_expm1(self):
        ans = self.solver.solve("expm1(100)")
        self.assertEqual(ans, round(math.expm1(100), 10))

    def test_exp(self):
        ans = self.solver.solve("exp(100)")
        self.assertEqual(ans, round(math.exp(100), 10))

    def test_fabs(self):
        ans = self.solver.solve("fabs(100)")
        self.assertEqual(ans, math.fabs(100))

    def test_factorial(self):
        ans = self.solver.solve("factorial(5)")
        self.assertEqual(ans, math.factorial(5))

    def test_floor(self):
        ans = self.solver.solve("floor(100.1)")
        self.assertEqual(ans, math.floor(100.1))

    def test_gamma(self):
        ans = self.solver.solve("gamma(100)")
        self.assertEqual(ans, round(math.gamma(100), 10))

    def test_lgamma(self):
        ans = self.solver.solve("lgamma(100)")
        self.assertEqual(ans, round(math.lgamma(100), 10))

    def test_log10(self):
        ans = self.solver.solve("log10(100)")
        self.assertEqual(ans, math.log10(100))

    def test_log1p(self):
        ans = self.solver.solve("log1p(100)")
        self.assertEqual(ans, round(math.log1p(100), 10))

    def test_log2(self):
        ans = self.solver.solve("log2(100)")
        self.assertEqual(ans, round(math.log2(100), 10))

    def test_radians(self):
        ans = self.solver.solve("radians(100)")
        self.assertEqual(ans, round(math.radians(100), 10))

    def test_sinh(self):
        ans = self.solver.solve("sinh(100)")
        self.assertEqual(ans, round(math.sinh(100), 10))

    def test_sin(self):
        ans = self.solver.solve("sin(100)")
        self.assertEqual(ans, round(math.sin(100), 10))

    def test_sqrt(self):
        ans = self.solver.solve("sqrt(100)")
        self.assertEqual(ans, round(math.sqrt(100), 10))

    def test_tanh(self):
        ans = self.solver.solve("tanh(100)")
        self.assertEqual(ans, round(math.tanh(100), 10))

    def test_tan(self):
        ans = self.solver.solve("tan(100)")
        self.assertEqual(ans, round(math.tan(100), 10))

    def test_trunc(self):
        ans = self.solver.solve("trunc(100.1)")
        self.assertEqual(ans, math.trunc(100.1))

    def test_atan2(self):
        ans = self.solver.solve("atan2(100, 10)")
        self.assertEqual(ans, round(math.atan2(100, 10), 10))

    def test_copysign(self):
        ans = self.solver.solve("copysign(1, -1)")
        self.assertEqual(ans, math.copysign(1, -1))

    def test_fmod(self):
        ans = self.solver.solve("fmod(100, 10)")
        self.assertEqual(ans, math.fmod(100, 10))

    def test_gcd(self):
        ans = self.solver.solve("gcd(64, 128)")
        self.assertEqual(ans, math.gcd(64, 128))

    def test_hypot(self):
        ans = self.solver.solve("hypot(100, 10)")
        self.assertEqual(ans, round(math.hypot(100, 10), 10))

    def test_ldexp(self):
        ans = self.solver.solve("ldexp(100, 10)")
        self.assertEqual(ans, round(math.ldexp(100, 10), 10))

    def test_log(self):
        ans = self.solver.solve("log(64, 2)")
        self.assertEqual(ans, math.log(64, 2))

    def test_pow(self):
        ans = self.solver.solve("pow(2, 6)")
        self.assertEqual(ans, math.pow(2, 6))

    def test_mod(self):
        ans = self.solver.solve("10 % 3")
        self.assertEqual(ans, 10 % 3)

    def test_exponent(self):
        ans = self.solver.solve("2 ** 6")
        self.assertEqual(ans, 2 ** 6)

    def test_bit_and(self):
        ans = self.solver.solve("1 & 3")
        self.assertEqual(ans, 1 & 3)

    def test_bit_or(self):
        ans = self.solver.solve("1 | 2")
        self.assertEqual(ans, 1 | 2)

    def test_bit_xor(self):
        ans = self.solver.solve("1 ^ 3")
        self.assertEqual(ans, 1 ^ 3)

    def test_left_shift(self):
        ans = self.solver.solve("1 << 1")
        self.assertEqual(ans, 1 << 1)

    def test_right_shift(self):
        ans = self.solver.solve("2 >> 1")
        self.assertEqual(ans, 2 >> 1)

    def test_missing_argument(self):
        with self.assertRaises(MissingArgumentError):
            self.solver.solve("100 +")

    def test_pi(self):
        ans = self.solver.solve("pi")
        self.assertEqual(ans, round(math.pi, 10))

    def test_e(self):
        ans = self.solver.solve("e")
        self.assertEqual(ans, round(math.e, 10))

    def test_inf(self):
        ans = self.solver.solve("inf")
        self.assertTrue(math.isinf(ans))

    def test_nan(self):
        ans = self.solver.solve("nan")
        self.assertTrue(math.isnan(ans))

    def test_tau(self):
        ans = self.solver.solve("tau")
        self.assertEqual(ans, round(math.tau, 10))

    def test_expression1(self):
        ans = self.solver.solve("9 - (1 + 2)")
        self.assertEqual(ans, 9 - (1 + 2))

    def test_expression2(self):
        ans = self.solver.solve("abs(-1)")
        self.assertEqual(ans, abs(-1))

    def test_expression3(self):
        ans = self.solver.solve("1 - abs(-1)")
        self.assertEqual(ans, 1 - abs(-1))

    def test_expression4(self):
        ans = self.solver.solve("(1) - (2)")
        self.assertEqual(ans, (1) - (2))

    def test_expression5(self):
        ans = self.solver.solve("-9 - 9")
        self.assertEqual(ans, -9 - 9)

    def test_expression6(self):
        ans = self.solver.solve("abs -1")
        self.assertEqual(ans, abs(-1))

    def test_expression7(self):
        ans = self.solver.solve("-pi")
        self.assertEqual(ans, round(-math.pi, 10))

    def test_expression8(self):
        ans = self.solver.solve("-abs(-pi)")
        self.assertEqual(ans, round(-abs(-math.pi), 10))

    def test_expression9(self):
        ans = self.solver.solve("9 - (-9) - (-9)")
        self.assertEqual(ans, 9 - (-9) - (-9))

    def test_expression10(self):
        ans = self.solver.solve("sin(-abs(-pi))")
        self.assertEqual(ans, round(math.sin(-abs(-math.pi)), 10))

    def test_expression11(self):
        ans = self.solver.solve("-(0 + 9)")
        self.assertEqual(ans, -(0 + 9))

    def test_expression12(self):
        ans = self.solver.solve("sin(-pi)")
        self.assertEqual(ans, round(math.sin(-math.pi), 10))


if __name__ == "__main__":
    main()
